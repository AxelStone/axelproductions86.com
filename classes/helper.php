<?php 

class Helper {

  static function postVal($str) {
    if (isset($_POST[$str])) {
      return $_POST[$str];
    } else {
      return;
    }
  }

  static function filter($what, $col, $val) {
    $resarr = [];
    foreach($what as $p) {
      if ($p[$col] == $val) {
        array_push($resarr, $p);
      }
    } return $resarr;
  }

  static function guidv4($data = null) {
    // Generate 16 bytes (128 bits) of random data or use the data passed into the function.
    $data = $data ?? random_bytes(16);
    assert(strlen($data) == 16);
    // Set version to 0100
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
    // Set bits 6-7 to 10
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
    // Output the 36 character UUID.
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
  }

  static function generateString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }

  static function parseUrl($name) {
    $parsed_url = str_replace('co.uk', 'co#uk', $name);
    $names = explode('.', $parsed_url);
    if (count($names) == 1) {
      $domain = $names[0];
      $subdomain = null;
      $full = $names[0];
      $ext = null;
    } elseif (count($names) == 2) {
      $ext = str_replace('#', '.', $names[1]);
      $domain = $names[0];
      $subdomain = null;
      $full = $domain.'.'.$ext;
    } else {
      $ext = str_replace('#', '.', $names[count($names)-1]);
      $domain = $names[count($names)-2];
      $subdomain = isset($names[count($names)-3]) ? $names[count($names)-3] : '';
      $full = $subdomain.'.'.$domain.'.'.$ext;
    }
    return ['domain' => $domain, 'subdomain' => $subdomain, 'domainext' => $ext != null ? $domain.'.'.$ext : null, 'full' => $full];
  }

  static function vardump($variable,$strlen=100,$width=25,$depth=10,$i=0,&$objects = array()) {
    $search = array("\0", "\a", "\b", "\f", "<br>", "\r", "\t", "\v");
    $replace = array('\0', '\a', '\b', '\f', '<br>', '\r', '\t', '\v');   
    $string = '';
   
    switch(gettype($variable)) {
      case 'boolean':      $string.= '<i style="color:red;">'.($variable?'true':'false').'</i>'; break;
      case 'integer':      $string.= '<span style="color:MEDIUMBLUE;">'.$variable.'</span>'; break;
      case 'double':       $string.= '<span style="color:MEDIUMBLUE;">'.$variable.'</span>'; break;
      case 'resource':     $string.= '[resource]';             break;
      case 'NULL':         $string.= "<i>null</i>";            break;
      case 'unknown type': $string.= '???';                    break;
      case 'string':
        $len = strlen($variable);
        $variable = str_replace($search,$replace,substr($variable,0,$strlen),$count);
        $variable = substr($variable,0,$strlen);
        if ($len<$strlen) $string.= '<span style="color:darkorange;">"'.$variable.'"</span>';
        else $string.= 'string('.$len.'): <span style="color:darkorange;">"'.$variable.'"</span>"...';
        break;
      case 'array':
        $len = count($variable);
        if ($i==$depth) $string.= 'array('.$len.') {...}';
        elseif(!$len) $string.= 'array(0) {}';
        else {
          $keys = array_keys($variable);
          $spaces = str_repeat('&nbsp;&nbsp;',$i*2);
          $string.= "array($len)".' [';
          $count=0;
          foreach($keys as $key) {
            if ($count==$width) {
              $string.= "<br>".$spaces."  ...";
              break;
            }
            $string.= "<br>".$spaces."&nbsp;&nbsp;[$key] => ";
            $string.= Helper::vardump($variable[$key],$strlen,$width,$depth,$i+1,$objects);
            $count++;
          }
          $string.="<br>".$spaces.']';
        }
        break;
      case 'object':
        $id = array_search($variable,$objects,true);
        if ($id!==false)
          $string.=get_class($variable).'#'.($id+1).' {...}';
        else if($i==$depth)
          $string.=get_class($variable).' {...}';
        else {
          $id = array_push($objects,$variable);
          $array = (array)$variable;
          $spaces = str_repeat('&nbsp;&nbsp;',$i*2);
          $string.= "obj <strong>".get_class($variable)."</strong>#$id ".$spaces.'{';
          $properties = array_keys($array);
          foreach($properties as $property) {
            $name = str_replace("\0",':',trim($property));
            $string.= "<br>".$spaces."&nbsp;&nbsp;<strong>$name</strong> => ";
            $string.= Helper::vardump($array[$property],$strlen,$width,$depth,$i+1,$objects);
          }
          $string.= "<br>".$spaces.'}';
        }
        break;
    }   
    if ($i>0) return $string;
    $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
    do $caller = array_shift($backtrace); while ($caller && !isset($caller['file']));
    if ($caller) $string = $string.'<br><i style="color:#666; font-size:12px; line-height:20px;">'.$caller['file'].':'.$caller['line']."</i>";
    echo '<div style="padding:5px; border:1px solid #bbb; background: #eee; font-size:14px;">'.$string.'</div><br>';
  }

}
