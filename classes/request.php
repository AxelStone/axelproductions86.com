<?php

class Request {
  function __construct() {
    $this->bootstrapSelf();
  }

  private function bootstrapSelf() {
    foreach ($_SERVER as $key => $value) {
      // echo $key.' - '.$value.' <br>'; 
      $this->{$this->toCamelCase($key)} = $value;
    }
  }

  private function toCamelCase($string) {
    $result = strtolower($string);
    preg_match_all('/_[a-z]/', $result, $matches);
    foreach ($matches[0] as $match) {
      $c = str_replace('_', '', strtoupper($match));
      $result = str_replace($match, $c, $result);
    }
    return $result;
  }

  public function getBody() {
    if ($this->requestMethod == "GET") {
      return;
    }

    if ($this->requestMethod == "POST") {
      $body = array();

      // API (json)
      if (strpos($_SERVER['CONTENT_TYPE'], 'application/json') !== false) {
    
        $json = file_get_contents('php://input');
        $json = str_replace("\t", '', $json);
        $data = json_decode($json, true);
        return $data;
      
      // form_data
      } else {
        foreach ($_POST as $key => $value) {
          $body[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
        }
        return $body;
      }


    }
  }
}
