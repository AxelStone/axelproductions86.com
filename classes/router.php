<?php

class Router {
  private $request;
  private $supportedHttpMethods = array("GET", "POST", "OPTIONS");

  /*
  private apipost = ['api/provision/feedback' => [callback, true]]
  */

  function __construct($request) {
    $this->request = $request;
  }

  function __call($name, $args) { 

    $route = $args[0];
    $method = $args[1];
    $accessRoles = isset($args[2]) ? $args[2] : true;

    if (!in_array(strtoupper(str_replace('api' , '', $name)), $this->supportedHttpMethods)) {
      $this->invalidMethodHandler();
    }
    $this->{strtolower($name)}[$this->formatRoute($route)] = [$method, $accessRoles];
  }

  /**
   * Removes trailing forward slashes from the right of the route and remove parameters.
   * @param route (string)
   */
  private function formatRoute($route) {
    $result = rtrim($route, '/');
    $result = strtok($result, '?');
    $result = rtrim($result, '/');
    
    if ($result == '' || $result == '/index.php') {
      return '/';
    }
    return $result;
  }

  private function invalidMethodHandler() {
    header("{$this->request->serverProtocol} 405 Method Not Allowed");
  }

  private function defaultRequestHandler() {
    header("{$this->request->serverProtocol} 404 Not Found");
  }

  /**
   * Resolves a route
   */
  function resolve() {

    if ($this->request->requestMethod == 'OPTIONS') {
      return;
    }

    if (
      isset($this->request->contentType) && 
      strpos($this->request->contentType, 'application/json') !== false
    ) {
      $methodDictionary = $this->{'api'.strtolower($this->request->requestMethod)};
      header('Content-type:application/json');
 
    } else {
      $methodDictionary = $this->{strtolower($this->request->requestMethod)};
    }

    $formatedRoute = $this->formatRoute($this->request->requestUri);
    $method = $methodDictionary[$formatedRoute][0];
    $accessRoles = $methodDictionary[$formatedRoute][1];

    if (is_null($method)) {
      $this->defaultRequestHandler();
      return;
    }

    // validate user access
    if (
      $accessRoles === true || 
      (Store::$user == null && $accessRoles === false) ||
      (Store::$user && is_array($accessRoles) && in_array(Store::$user['role'], $accessRoles)) ||
      (Store::$user && is_string($accessRoles) && Store::$user['role'] == $accessRoles)
    ) {
      Store::$request = $this->request;
      call_user_func($method);
    } else {
      echo 'Current user does not have access to this url!';
    }
  }

  function __destruct() {
    $this->resolve();
  }
}
