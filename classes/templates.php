<?php 

include_once('views/head.php');
include_once('views/layouts/basicLayout.php');
include_once('views/layouts/rule_editor.php');
include_once('views/layouts/axel.php');
include_once('views/pages/homepage.php');
include_once('views/pages/logodesign.php');
include_once('views/pages/account_activation.php');
include_once('views/pages/privacy_policy.php');
include_once('views/components/header.php');
include_once('views/components/footer.php');
include_once('views/components/navigation.php');  
include_once('views/components/updates.php');  

class Templates {

  static $headHTML = '';
  static $bodyHTML = '';

  static function render() {

    switch (Store::$page) {

      case 'home-page':

        Templates::$headHTML = viewHead('Axel Productions 86');
        Templates::$bodyHTML = viewHomepage();        
        break;

      case 'account-activation':
        
        Templates::$headHTML = viewHead('Axel Productions 86');
        Templates::$bodyHTML = viewAccountActivation(); 
        break;

      case 'logodesign':

        Templates::$headHTML = viewHead('Axel Productions 86');
        Templates::$bodyHTML = viewLogodesign();        
        break;

      case 'privacy-policy':
        
        Templates::$headHTML = viewHead('Axel Productions 86 | Privacy Policy');
        Templates::$bodyHTML = viewPrivacyPolicy();
        break;

      case 'log-in':
        
        Templates::$headHTML = viewHead('Axel Productions');
        Templates::$bodyHTML = viewLogIn(); 
        break;
      
      default:
        echo '<h2>View error - no page view</h2>';
    }
    
    Templates::renderHTML();

  }

  private static function renderHTML() {
    echo '<!DOCTYPE html>
    <html lang="en">
      <head>
        '.Templates::$headHTML.'      
      </head>
        '.Templates::$bodyHTML.'
    </html>';
  }

  static function renderReactSPA($url) {
    echo file_get_contents($url. '/index.html');
  }

  static function renderScrSPA($url) {
    echo file_get_contents($url. '/index.html');
  }

}