<?php

include_once 'config.php';
include_once 'constants.php';
include_once 'classes/helper.php';
include_once 'classes/store.php';
include_once 'classes/request.php';
include_once 'classes/router.php';
include_once 'classes/templates.php';
include_once 'controllers/main.php';
include_once 'controllers/api.php';
include_once 'controllers/user.php';
include_once 'models/user.php';
include_once 'models/api.php';
require_once 'vendors/google-api-php-client-php8/vendor/autoload.php';


header("Access-Control-Allow-Headers: Access-Control-Allow-Origin, Content-Type, Access-Control-Allow-Headers, X-Requested-With");
/*
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
 */

// session_start();

$router = new Router(new Request);
// Store::$user = isset($_SESSION['loggedUser']) ? $_SESSION['loggedUser'] : null;

$router->get('/', function() {
  MainController::Homepage();
});

$router->get('/privacy-policy', function() {
  MainController::PrivacyPolicy();
});

$router->get('/provision-rule-editor', function() {
  Templates::renderReactSPA(dirname(__FILE__).'/assets/rule_editor');   
});

$router->get('/account-activation', function() {
  UserController::AccountActivation();
});

$router->get('/scr', function() {
  Templates::renderReactSPA(dirname(__FILE__).'/assets/scr');   
});


/**
 * API - proVision Chrome Extension
 */
$router->apipost('/api/provision/feedback', function() {
  APIController::postFeedback();
});

$router->apipost('/api/provision/findrules', function() {
  APIController::findRules();
});

/**
 * API - proVision Rule Editor
 */
$router->apipost('/api/provision/searchrulesets', function() {
  APIController::searchRulesets();
});

$router->apipost('/api/provision/getruleset', function() {
  APIController::postGetRuleset();
});

$router->apipost('/api/provision/validaterulesetname', function() {
  APIController::postValidateRulesetName();
});

$router->apipost('/api/provision/createnewruleset', function() {
  APIController::postNewRuleset();
});

$router->apipost('/api/provision/createupdate', function() {
  APIController::postCreateUpdate();
});

$router->apipost('/api/provision/editruleset', function() {
  APIController::postEditRuleset();
});

$router->apipost('/api/provision/getuserposts', function() {
  APIController::getUserPosts();
});

$router->apipost('/api/provision/getadminposts', function() {
  APIController::getAdminPosts();
});

$router->apipost('/api/provision/deletepost', function() {
  APIController::deletePost();
});

$router->apipost('/api/provision/adminresolvepost', function() {
  APIController::adminResolvePost();
});

/**
 * Login/Register/Logout 
 */
$router->apipost('/api/login', function() {
  UserController::APILogin();
});

$router->apipost('/api/googlelogin', function() {
  UserController::APIGoogleLogin();
});

$router->apipost('/api/register', function() {
  UserController::APIRegister();
});

$router->apipost('/api/logout', function() {
  UserController::APILogout();
});

$router->apipost('/api/validateuser', function() {
  UserController::APIValidateToken();
});

$router->apipost('/api/changepassword', function() {
  UserController::APIChangePwd();
});

$router->apipost('/api/regeneratepassword', function() {
  UserController::APIRegeneratePwd();
});


$router->options(null, function() {
  return;
});



/*
$router->get('/sign-up', function() {
  Store::$page = 'sign-up';
  Templates::render();
}, false);

$router->post('/sign-up', function() {
  UserController::PostSignUp();
}, false);

$router->get('/log-in', function() {
  Store::$page = 'log-in';
  Templates::render();
}, false);

$router->post('/log-in', function() {
  UserController::PostLogIn();
}, false);

$router->get('/log-out', function() {
  UserController::LogOut();
}, ALL_ROLES);
*/
