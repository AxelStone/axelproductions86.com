<?php 

class MainController {

  static function Homepage() {
    
    Store::$page = 'home-page';
    Templates::render();
    // unset($_SESSION['userJustCreated']);
    // unset($_SESSION['userJustLoggedOut']);
  }

  static function Logodesign() {

    Store::$page = 'logodesign';
    Templates::render();

  }

  static function PrivacyPolicy() {

    Store::$page = 'privacy-policy';
    Templates::render();
  }

  static function validateAccess($vals, $validateToken = false, $role = false) {

    if ($vals['secret'] != SECRET) {
      http_response_code(401);
      echo 'Not authorized';
      return false;
    }

    $user = null;
    if ($validateToken) {
      $user = UserController::ValidateToken($vals['token']);
      if (!$user) {
        echo json_encode(['error' => ['code' => '0', 'text' => 'Something went wrong!']]);
        return false;
      }
    }

    if ($role !== false) {
      if ($user['roles'] != $role) {
        echo json_encode(['error' => ['code' => '10', 'text' => 'Access denied!']]);
        return false;
      }
    }

    return $user !== null ? $user : true;
  }

}