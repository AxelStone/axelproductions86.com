<?php 

class APIController {

  static function postFeedback() {
    $vals = Store::$request->getBody();
    $res = APIModel::saveFeedback($vals);
    echo json_encode($res);
  }

  static function findRules() {
    $vals = Store::$request->getBody();
    $ok = MainController::validateAccess($vals);
    if ($ok) {
      $parsed = Helper::parseUrl($vals['url']);
      $res = APIModel::findRules($parsed['domain'], $parsed['subdomain'], $parsed['domainext'], $parsed['full']);
      echo json_encode($res);
    }
  }

  static function searchRulesets() {
    $vals = Store::$request->getBody();
    $ok = MainController::validateAccess($vals);
    if ($ok) {
      $res = APIModel::getRules($vals['search'] ? $vals['search'] : null);
      echo json_encode($res);
    }
  }

  static function postGetRuleset() {
    $vals = Store::$request->getBody();
    $ok = MainController::validateAccess($vals);
    if ($ok) {
      $res = APIModel::getRuleset($vals['id'], $vals['what']);
      echo json_encode($res);
    } 
  }

  static function postValidateRulesetName() {
    $vals = Store::$request->getBody();
    if ($vals['secret'] == SECRET) {

      $err = false; 
      $parsed = Helper::parseUrl($vals['name']);
      $rules = APIModel::findPosts($parsed['domain'], $parsed['subdomain'], $parsed['full']);
      if (count($rules) > 0) {
        for ($i=0; $i<count($rules); $i++) {
          if ($rules[$i]['name'] == $parsed['full']) {
            // only if we are not updating existing ruleset with the same name
            if ($vals['update'] !== $rules[$i]['name']) {
              $err = true;
            }
          }
        }
      }

      if ($err) {
        echo json_encode(['error' => ['code' => '6', 'text' => 'This ruleset name already exists!'], 'posts' => $rules]);
      } else {
        echo json_encode(['posts' => $rules]);
      }
    } else {
      http_response_code(401);
      return 'Not authorized';
    }
  }

  static function postNewRuleset() {
    $vals = Store::$request->getBody();
    if ($vals['secret'] == SECRET) {
      $user = UserController::ValidateToken($vals['token'], ClientType::Editor);
      if (ApiModel::getRulesetByName($vals['ruleset']['name']) == []) {
        $vals = $vals['ruleset'];
        if ($user) {
          if ($user['roles'] == '0') {
            ApiModel::createSuggestion($vals['name'], $vals['json'], $vals['mature'], $user['id']);
            //ApiModel::createRuleset($vals['name'], $vals['json'], $vals['mature'], $user['id']);
          } else if ($user['roles'] == '1') {
            ApiModel::createSuggestion($vals['name'], $vals['json'], $vals['mature'], $user['id']);
          }
          echo json_encode('ok');
        }
      } else {
        echo json_encode(['error' => ['code' => '6', 'text' => 'This ruleset name already exists!']]);
      }

    } else {
      http_response_code(401);
      return 'Not authorized';
    }
  }

  static function postCreateUpdate() {
    $vals = Store::$request->getBody();
    $user = MainController::validateAccess($vals, true);
    if ($user) {
      $vals = $vals['ruleset'];
      ApiModel::createUpdate($vals['name'], $vals['json'], $vals['mature'], $vals['id'], $user['id']);
    }
    echo json_encode('ok');
  }

  static function getUserPosts() {
    $vals = Store::$request->getBody();
    $user = MainController::validateAccess($vals, true);
    if ($user) {
      $res = APIModel::getUserPosts($user['id']);
      echo json_encode(['posts' => $res]);
    } else {
      echo json_encode(['error' => ['code' => '0', 'text' => 'Something went wrong!']]);
    }
  }

  static function getAdminPosts() {
    $vals = Store::$request->getBody();
    $user = MainController::validateAccess($vals, true, 0);
    if ($user) {
      $res = APIModel::getAdminPosts();
      echo json_encode(['posts' => $res]);
    } else {
      echo json_encode(['error' => ['code' => '0', 'text' => 'Something went wrong!']]);
    }
  }

  static function deletePost() {
    $vals = Store::$request->getBody();
    $user = MainController::validateAccess($vals, true);
    if ($user) {
      $ok = APIModel::deletePost($vals['id'], $user['id']);
      if ($ok) {
        $posts = $user['roles'] == '0' ? APIModel::getAdminPosts() : APIModel::getUserPosts($user['id']);
        echo json_encode(['posts' => $posts]);
      } else {
        echo json_encode(['error' => ['code' => '0', 'text' => 'Something went wrong!']]);
      }
    }     
  }

  static function postEditRuleset() {
    $vals = Store::$request->getBody();
    $user = MainController::validateAccess($vals, true);
    if ($user) {
      $ok = APIModel::editPost($vals['ruleset']);
      echo json_encode('ok');
    }
  }

  static function adminResolvePost() {
    $vals = Store::$request->getBody();
    $user = MainController::validateAccess($vals, true, 0);
    if ($user) {
      $ok = APIModel::resolvePost($vals['ruleset'], $vals['result'], $user);
      echo json_encode('ok');
    }
  }
}