<?php 

include_once('views/emails/account_activation.php');
include_once('views/emails/password_recovery.php');

class UserController {

  /**
   * Get user from db, validate pwd, create or refresh token, send token and user info
   */
  static function APILogin() {
    $vals = Store::$request->getBody();
    !isset($vals['type']) ? $vals['type'] = ClientType::Editor : null;
    $user = UserModel::getUserByEmail($vals['email']);

    // wrong email/password response
    if ($user == null || md5($vals['password']) == $user['password']) {
      echo json_encode(['error' => ErrorTypes::WrongEmailPassword]);
    // user not activated response
    } else if ($user['status'] == UserStatuses::NotActivated) {
      echo json_encode(['error' => ErrorTypes::AccountNotActivated]);
    // successful login response
    } else {
      $token = UserModel::regenerateToken($user['id'], $vals['type']);
      echo json_encode(['user' => $user, 'token' => $token]);
    }

  }

  static function APIGoogleLogin() {
    $vals = Store::$request->getBody();
    !isset($vals['type']) ? $vals['type'] = ClientType::Editor : null;
    // Specify the CLIENT_ID of the app (https://console.cloud.google.com/apis/credentials) that accesses the backend
    $client = new Google_Client([
      'client_id' => $vals['type'] == ClientType::Extension ? 
        '457312517099-90qhv4vtncd8cmpp95a2hmtheo4lad5f.apps.googleusercontent.com' :
        '457312517099-4k8un09ekla150rafg5me687s47irl72.apps.googleusercontent.com'
        
    ]);
    $payload = $client->verifyIdToken($vals['id_token']);
    if ($payload) {
      $guserid = $payload['sub'];
      $user = UserModel::getUserByGoogleUserId($guserid);
      if (!$user) {
        $uid = UserModel::createGoogleUser($payload);
        $user = UserModel::getUserById($uid);
        $token = UserModel::regenerateToken($uid, $vals['type']);
        echo json_encode(['user' => $user, 'token' => $token]);
      } else {
        $token = UserModel::regenerateToken($user['id'], $vals['type']);
        echo json_encode(['user' => $user, 'token' => $token]);
      }
    } else {
      echo json_encode(['error' => ErrorTypes::UnsuccessfulGoogleLogin]);
    }
  }

  static function APIRegister() {
    $vals = Store::$request->getBody();
    $user = UserModel::getUserByEmail($vals['email']);
    if (!$user) {
      $userId = UserModel::createLocalUser($vals['email'], $vals['nick'], $vals['password']);
      $user = UserModel::getUserById($userId);
      unset($user['password']);

      $headers[] = 'MIME-Version: 1.0';
      $headers[] = 'Content-type: text/html; charset=iso-8859-1';
      $headers[] = 'From: Axel Productions 86 <axelproductions86@gmail.com>';
      $headers[] = 'Bcc: axelproductions86@gmail.com';
      // send mail
      mail($vals['email'], 'proVision - account activation', viewAccountActivationEmail($user), implode("\r\n", $headers));
      // $token = UserModel::regenerateToken($user['id']);
      echo json_encode($user);
    } else {
      echo json_encode(['error' => ErrorTypes::UserAlreadyExists]);
    }
  }

  static function APILogout() {
    $vals = Store::$request->getBody();
    $res = UserModel::deleteToken($vals['token']);
    echo json_encode($res);
  }

  static function APIValidateToken() {
    $vals = Store::$request->getBody();
    !isset($vals['type']) ? $vals['type'] = ClientType::Editor : null;
    $user = UserController::ValidateToken($vals['token'], $vals['type']);
    echo json_encode($user);
  }

  static function APIChangePwd() {
    $vals = Store::$request->getBody();
    $res = UserModel::changePassword($vals['uid'], $vals['oldpwd'], $vals['newpwd']);
    if ($res) {
      echo json_encode($res);
    } else {
      echo json_encode(['error' => ErrorTypes::WrongPassword]);
    }
  }

  static function APIRegeneratePwd() {
    $vals = Store::$request->getBody();
    $newpwd = UserModel::regeneratePassword($vals['email']);
    if ($newpwd) {
      $headers[] = 'MIME-Version: 1.0';
      $headers[] = 'Content-type: text/html; charset=iso-8859-1';
      $headers[] = 'From: Axel Productions 86 <axelproductions86@gmail.com>';
      $headers[] = 'Bcc: axelproductions86@gmail.com';
      // send mail
      mail($vals['email'], 'proVision - password recovery', viewPasswordRecoveryEmail($newpwd), implode("\r\n", $headers));
      echo json_encode(true);
    } else {
      echo json_encode(['error' => ErrorTypes::WrongEmail]);
    }
  }


  static function AccountActivation() {
    $user = UserModel::getUserByEmail(base64_decode($_GET['code']));
    if ($user) {
      Store::$user = $user; 
      UserModel::activateUser($user['id']);
      Store::$page = 'account-activation';
      Templates::render();
    } else {
      echo 'Error! User does not exists.';
    }
  }

  static function ValidateToken($token, $type = ClientType::Editor) {
    $token = UserModel::getTokenByToken($token, $type);
    if (!$token || $token['valid_until'] < time()) {
      return false;
    } else {
      $user = UserModel::getUserById($token['user_id']);
      return($user);
    }
  }

}