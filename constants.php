<?php 

global $pdo;
$pdo = new PDO(DB_DNS, DB_USER, DB_PASS);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

const USERNAME_MIN_LENGTH = 5;
const PASSWORD_MIN_LENGTH = 5;
const NAVIGATION = [];

abstract class UserStatuses {
  const NotActivated = 0;
  const Active = 1;
}

abstract class ClientType {
  const Editor = 1;
  const Extension = 2;
}

abstract class ErrorTypes {
  const WrongEmailPassword = ['code' => '1', 'text' => 'Wrong username or password'];
  const UserAlreadyExists = ['code' => '2', 'text' => 'User already exists'];
  const AccountNotActivated = ['code' => '3', 'text' => 'Account is not activated'];
  const WrongPassword = ['code' => '4', 'text' => 'Wrong password'];
  const WrongEmail = ['code' => '5', 'text' => 'Wrong email'];
  const UnsuccessfulGoogleLogin = ['code' => '10', 'text' => 'Unsuccessful google login'];
}