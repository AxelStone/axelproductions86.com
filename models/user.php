<?php

class UserModel {

  static function createLocalUser($email, $nick, $pwd) {
    $pwd = md5($pwd);
    
    global $pdo;
    $st = $pdo->prepare('INSERT INTO users VALUES (NULL, NULL, :type, :nick, :pwd, :email, :status, :roles, :created)');
    $st->execute([
      'type' => 1, 'nick' => $nick, 'pwd' => $pwd, 
      'email' => $email, 'status' => 0,
      'roles' => '1', 'created' => time()
    ]);
    return $pdo->lastInsertId();
  }

  static function createGoogleUser($gusr) {

    global $pdo;
    $st = $pdo->prepare('INSERT INTO users VALUES (NULL, :userid, :type, :nick, :pwd, :email, :status, :roles, :created)');
    $st->execute([
      'userid' => $gusr['sub'],'type' => 2, 'nick' => $gusr['name'], 
      'pwd' => null, 'email' => $gusr['email'], 'status' => 1,
      'roles' => '1', 'created' => time()
    ]);
    return $pdo->lastInsertId();
  }

  static function activateUser($uid) {
    global $pdo;
    $st = $pdo->prepare('UPDATE users SET status=1 WHERE id=:uid');
    $st->execute(['uid' => $uid]);
  }

  static function getUserById($uid) {
    global $pdo;
    $st = $pdo->prepare('SELECT * FROM users WHERE id = :uid');
    $st->execute(['uid' => $uid]);
    $user = $st->fetch(PDO::FETCH_ASSOC);
    return $user;
  }

  static function getUserByGoogleUserId($guid) {
    global $pdo;
    $st = $pdo->prepare('SELECT * FROM users WHERE user_id = :guid');
    $st->execute(['guid' => $guid]);
    $user = $st->fetch(PDO::FETCH_ASSOC);
    return $user;
  }

  static function getUserByEmail($email) {
    global $pdo;
    $st = $pdo->prepare('SELECT * FROM users WHERE email = :email AND type = 1');
    $st->execute(['email' => $email]);
    $user = $st->fetch(PDO::FETCH_ASSOC);
    return $user;
  }

  static function getUserList() {
    global $pdo;
    $st = $pdo->prepare('SELECT * FROM users ORDER BY nickname ASC');
    $st->execute();
    return $st->fetchAll(PDO::FETCH_ASSOC);;
  }

  static function regenerateToken($uid, $type) {
    $token = Helper::guidv4();

    global $pdo;
    $tok = UserModel::getTokenByUserId($uid, $type);

    if ($tok) {
      $st = $pdo->prepare('UPDATE tokens SET token = :token, valid_until = :until WHERE user_id = :userid AND type = :type');
      $st->execute(['userid' => $uid, 'token' => $token, 'until' => time() + 60*60*24*30, 'type' => $type]);
    } else {
      $st = $pdo->prepare('INSERT INTO tokens VALUES (null, :userid, :type, :token, :until)');
      $st->execute(['userid' => $uid, 'type' => $type, 'token' => $token, 'until' => (time() + 60*60*24*30)]);
    }

    return $token;
  }

  static function getTokenByUserId($uid, $type) {
    global $pdo;
    $st = $pdo->prepare('SELECT * FROM tokens WHERE user_id = :userid AND type = :type');
    $st->execute(['userid' => $uid, 'type' => $type]);
    $tok = $st->fetch(PDO::FETCH_ASSOC);
    return $tok;
  }

  static function getTokenByToken($token, $type) {
    global $pdo;
    $st = $pdo->prepare('SELECT * FROM tokens WHERE token = :token AND type = :type');
    $st->execute(['token' => $token, 'type' => $type]);
    $tok = $st->fetch(PDO::FETCH_ASSOC);
    return $tok;
  }

  static function deleteToken($token) {
    global $pdo;
    $st = $pdo->prepare('DELETE FROM tokens WHERE token = :token');
    $st->execute(['token' => $token]);
    return true;
  }

  static function updateUserStatus($uid, $userstatus) {
    global $pdo;
    $st = $pdo->prepare('UPDATE users SET status=:status WHERE id=:uid');
    $st->execute(['status' => $userstatus, 'uid' => $uid]);
  }

  static function changePassword($uid, $oldpwd, $newpwd) {
    global $pdo;
    $st = $pdo->prepare('SELECT * FROM users WHERE id = :uid');
    $st->execute(['uid' => $uid]);
    $user = $st->fetch(PDO::FETCH_ASSOC);
    if ($user && $user['password'] == md5($oldpwd)) {
      $st = $pdo->prepare('UPDATE users SET password=:pwd WHERE id=:uid');
      $st->execute(['pwd' => md5($newpwd), 'uid' => $uid]);
      return true;
    } else {
      return false;
    }
  }

  static function regeneratePassword($email) {
    global $pdo;
    $st = $pdo->prepare('SELECT * FROM users WHERE email = :email AND type = 1');
    $st->execute(['email' => $email]);
    $user = $st->fetch(PDO::FETCH_ASSOC);
    if ($user) {
      $newpwd = Helper::generateString(10);
      $st = $pdo->prepare('UPDATE users SET password=:pwd WHERE id=:uid');
      $st->execute(['pwd' => md5($newpwd), 'uid' => $user['id']]);
      return $newpwd;
    } else {
      return false;
    }
  }

}