<?php

class APIModel {

  static function saveFeedback($vals) {  
    global $pdo;
    $st = $pdo->prepare('INSERT INTO user_feedback VALUES (NULL, :userid, :url, :text, NULL)');
    $ok = $st->execute(['userid' => $vals['data']['userid'], 'url' => $vals['data']['url'], 'text' => $vals['data']['text']]);
    return $ok;
  } 

  static function findRules($domain, $subdomain, $domainext, $full) {
   
    global $pdo;
    try {
      $st = $pdo->prepare('SELECT * FROM rulelist WHERE name IN (:subdomain, :domain, :domainext, :full)');
      $ok = $st->execute(['subdomain' => $subdomain, 'domain' => $domain, 'domainext' => $domainext,'full' => $full]);
      $res = $st->fetchAll(PDO::FETCH_ASSOC);
    } catch(PDOException $e) { 
      http_response_code(400);
      echo $e; 
    }
    
    if (count($res) == 0) {
      // return empty ruleset to be cached in extension
      return [['name' => $full, 'rules' => []]];
    } else {
      // process found rules
      foreach($res as $i => $rule) {
        $res[$i]['rules'] = json_decode($rule['rules']);
        $res[$i]['for'] = $full;
      }
      return $res;
    }
  }

  static function getRules($search = null) { // Helper::vardump($search);
    global $pdo;
    try {
      if ($search !== null && strlen($search) > 0) {
        $st = $pdo->prepare("SELECT * FROM rulelist WHERE name LIKE CONCAT('%', :search, '%') ORDER BY name ASC");
        $ok = $st->execute(['search' => $search]);
      } else {
        $st = $pdo->prepare("SELECT * FROM rulelist ORDER BY name ASC");
        $ok = $st->execute();
      }
      $res = $st->fetchAll(PDO::FETCH_ASSOC);
      return $res;
    } catch(PDOException $e) { 
      http_response_code(400);
      return $e; 
    }
  }

  static function getRuleset($id, $what) { 
    global $pdo;
    if ($what === 'ruleset') {
      $st = $pdo->prepare("SELECT r.*, u.nickname FROM rulelist r LEFT JOIN users u ON r.author_id = u.id WHERE r.id = :id");
      $ok = $st->execute(['id' => $id]);
      $res = $st->fetch(PDO::FETCH_ASSOC);
      $st = $pdo->prepare("SELECT nickname FROM users WHERE id = :id");
      $ok = $st->execute(['id' => $res['modified_by_id']]);
      $r = $st->fetch(PDO::FETCH_ASSOC);
      $res['modby_nickname'] = $r ? $r['nickname'] : null;
    } else {
      $st = $pdo->prepare("SELECT s.*, u.nickname FROM posts s LEFT JOIN users u ON s.author_id = u.id WHERE s.id = :id");
      $ok = $st->execute(['id' => $id]);
      $res = $st->fetch(PDO::FETCH_ASSOC);
    } 
    
    return $res;
  }

  static function findPosts($domain, $subdomain, $full) { 
     
    global $pdo;
    try {
      $st = $pdo->prepare('SELECT * FROM rulelist WHERE 
        name LIKE CONCAT("%", :subdomain, "%") OR 
        name LIKE CONCAT("%", :domain, "%") OR 
        name LIKE CONCAT("%", :full, "%")'
      );
      $ok = $st->execute(['subdomain' => $subdomain, 'domain' => $domain, 'full' => $full]);
      $res = $st->fetchAll(PDO::FETCH_ASSOC);
    } catch(PDOException $e) { 
      http_response_code(400);
      echo $e; 
    }
    
    // process found rules
    foreach($res as $i => $rule) {
      $res[$i]['rules'] = json_decode($rule['rules']);
      $res[$i]['for'] = $full;
    }
    return $res;
  }

  static function getRulesetByName($name) {
    global $pdo;
    $st = $pdo->prepare("SELECT * FROM rulelist WHERE name = :search");
    $ok = $st->execute(['search' => $name]);
    $res = $st->fetchAll(PDO::FETCH_ASSOC);
    return $res;
  }

  static function createRuleset($name, $json, $mature, $uid) {

    global $pdo;
    try {
      $st = $pdo->prepare('INSERT INTO rulelist VALUES (NULL, :name, :rules, :author_id, :status, :adult, :created, NULL, NULL)');
      $ok = $st->execute([
        'name' => $name, 'rules' => $json, 'adult' => $mature ? 1 : 0, 'author_id' => $uid, 'status' => 1, 'created' => time()
      ]);
    } catch(PDOException $e) { 
      http_response_code(400);
      echo $e; 
    }
  }

  static function createUpdate($name, $json, $mature, $rid, $uid) {
    global $pdo;
    $st = $pdo->prepare('INSERT INTO posts VALUES (NULL, :rid, :name, :rules, :adult, :author_id, :status, :created)');
    $ok = $st->execute([
      'rid' => $rid, 'name' => $name, 'rules' => $json, 'adult' => $mature ? 1 : 0, 'author_id' => $uid, 'status' => 0, 'created' => time()
    ]);
  }

  static function createSuggestion($name, $json, $mature, $uid) {

    global $pdo;
    try {
      $st = $pdo->prepare('INSERT INTO posts VALUES (NULL, NULL, :name, :rules, :adult, :author_id, :status, :created)');
      $ok = $st->execute([
        'name' => $name, 'rules' => $json, 'adult' => $mature ? 1 : 0, 'author_id' => $uid, 'status' => 0, 'created' => time()
      ]);

    } catch(PDOException $e) { 
      http_response_code(400);
      echo $e; 
    }

  }

  static function getUserPosts($uid) {
    global $pdo;
    $st = $pdo->prepare("SELECT * FROM posts WHERE author_id = :uid ORDER BY created DESC");
    $ok = $st->execute(['uid' => $uid]);
    $res = $st->fetchAll(PDO::FETCH_ASSOC);
    return $res;
  }

  static function getAdminPosts() {
    global $pdo;
    $st = $pdo->prepare("SELECT * FROM posts WHERE status = 0 ORDER BY created ASC");
    $ok = $st->execute();
    $res = $st->fetchAll(PDO::FETCH_ASSOC);
    return $res;
  }

  static function deletePost($pid, $uid) {
    global $pdo;
    $st = $pdo->prepare("DELETE FROM posts WHERE author_id = :uid AND id = :pid");
    $count = $st->execute(['uid' => $uid, 'pid' => $pid]);
    return $count > 0 ? true : false;
  }

  static function editPost($ruleset) {
    global $pdo;
    $st = $pdo->prepare("UPDATE posts SET name = :name, rules = :json, adult = :adult WHERE id = :id AND status = 0");
    $ok = $st->execute(['name' => $ruleset['name'], 'json' => $ruleset['json'], 'adult' => $ruleset['mature'] ? 1 : 0 , 'id' => $ruleset['id']]);
    return $ok;
  }

  static function resolvePost($ruleset, $result) {
    global $pdo;
    
    $st = $pdo->prepare("SELECT * FROM posts WHERE id = :id AND status = 0");
    $ok = $st->execute(['id' => $ruleset['id']]);
    $res = $st->fetch(PDO::FETCH_ASSOC);
    if ($res) {
      $st = $pdo->prepare("UPDATE posts SET status = :status WHERE id = :id");
      $ok = $st->execute(['id' => $ruleset['id'], 'status' => $result ? 1 : 2]);
      if (!$result) { return; }
      if ($res['ruleset_id'] !== null) {
        $st = $pdo->prepare("UPDATE rulelist SET name = :name, rules = :json, adult = :adult, modified = :modified, modified_by_id = :modby WHERE id = :id");
        $ok = $st->execute([
          'name' => $ruleset['name'], 
          'json' => $ruleset['json'], 
          'adult' => $ruleset['mature'] ? 1 : 0, 
          'modified' => time(),
          'modby' => $res['author_id'],
          'id' => $res['ruleset_id']
        ]);
      } else {
        APIModel::createRuleset($ruleset['name'], $ruleset['json'], $ruleset['mature'], $res['author_id']);
      }
    
    }

    return $ok;
  }

}