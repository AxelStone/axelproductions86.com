class AxelSlider {
    static init(selector, userOptions = {}) {
        let element = document.querySelector(selector);
        let pages = Array.from(element.querySelectorAll('.page'));
        let options = Object.assign({}, this.defOptions, userOptions);
        let currentPage = options.startAt;
        element.insertAdjacentHTML('beforeend', `<div class="pagination">
      <button class="arrow left">&#5176;</button>
      <span class="page">
        <span class="current">${currentPage + 1}</span>/<span class="total">${pages.length}</span>
      </span>
      <button class="arrow right">&#5171;</button>
    </div>`);
        let slider = {
            element: element,
            pages: pages,
            options: options,
            currentPage: currentPage,
            elArrowLeft: element.querySelector('.pagination .left'),
            elArrowRight: element.querySelector('.pagination .right'),
            elCurrentPage: element.querySelector('.pagination .current'),
            elPageTotal: element.querySelector('.pagination .total')
        };
        let index = this.sliders.push(slider) - 1;
        slider.pages.forEach((pageEl, index) => {
            if (index === slider.currentPage) {
                pageEl.style.display = 'block';
            }
            else {
                pageEl.style.display = 'none';
            }
        });
        this.setArrows(slider);
        slider.elArrowLeft.addEventListener('click', (ev) => {
            this.slideTo(slider.currentPage - 1, index);
        });
        slider.elArrowRight.addEventListener('click', (ev) => {
            this.slideTo(slider.currentPage + 1, index);
        });
        return slider;
    }
    static slideTo(pageIndex, indexOrName = 0) {
        let slider = this.getSlider(indexOrName);
        if (pageIndex <= slider.pages.length - 1 && pageIndex >= 0) {
            slider.currentPage = pageIndex;
            slider.elCurrentPage.innerHTML = (pageIndex + 1).toString();
            this.setArrows(slider);
            slider.pages.forEach((pageEl, index) => {
                if (index === slider.currentPage) {
                    pageEl.style.display = 'block';
                }
                else {
                    pageEl.style.display = 'none';
                }
            });
        }
    }
    static getSlider(indexOrName) {
        if (typeof indexOrName === 'string') {
            return this.sliders.find((sl) => sl.options.name === indexOrName);
        }
        else {
            return this.sliders[indexOrName];
        }
    }
    static setArrows(slider) {
        if (slider.currentPage === 0) {
            slider.elArrowLeft.classList.add('disabled');
        }
        else {
            slider.elArrowLeft.classList.remove('disabled');
        }
        if (slider.currentPage === slider.pages.length - 1) {
            slider.elArrowRight.classList.add('disabled');
        }
        else {
            slider.elArrowRight.classList.remove('disabled');
        }
    }
}
AxelSlider.sliders = [];
AxelSlider.defOptions = {
    name: null,
    startAt: 0
};
