NodeList.prototype.forEach = Array.prototype.forEach;
document.onreadystatechange = function () {
  if (document.readyState == "interactive") {
    
    let slider = AxelSlider.init('.slider', {})

    const tabbut1 = document.getElementById('button-about');
    const tabbut2 = document.getElementById('button-devlog');
    const tab1 = document.getElementById('tab-about')
    const tab2 = document.getElementById('tab-devlog')
    tabbut1.addEventListener('click', function() {
      tab1.classList.remove('hidden');
      tab2.classList.add('hidden');
      tabbut1.classList.add('active');
      tabbut2.classList.remove('active');
      
    })
    tabbut2.addEventListener('click', function() {
      tab1.classList.add('hidden');
      tab2.classList.remove('hidden');
      tabbut1.classList.remove('active');
      tabbut2.classList.add('active');
    })
  }
  if (document.readyState == "complete") {
    // do stuff
  }
}



