<?php

function viewHead($title) {
  return '
    <meta charset="utf-8">
    <title>'.$title.'</title>
    <link rel="icon" href="/assets/images/favicon.png" />
    <meta name="description" content="Developing community driven cookie consents and other annoying stuff removal tool.">
    <meta name="keywords" content="proVision Chrome Extension, cookie consent, push notifications, subscribe prompts, turn-off-your-adblocker, removal tool">
    <meta name="author" content="Axel Productions 86">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"">
    <meta property="og:image:url" content="https://www.axelproductions86.com/assets/images/provision_640x400_banner.jpg">
    <meta property="og:image:secure_url" content="https://www.axelproductions86.com/assets/images/provision_640x400_banner.jpg">
    <link href="/assets/css/axel.css?v='.time().'" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Catamaran:wght@300;400;500;600&family=Marck+Script&family=Vast+Shadow&family=Audiowide&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <script src="https://kit.fontawesome.com/1aeaebf775.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="/assets/js/axelslider/axelslider.js"></script>
    <script type="text/javascript" src="/assets/js/js.js?v=00001"></script>
  ';
}