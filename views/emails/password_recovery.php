<?php 
function viewPasswordRecoveryEmail($pwd) {

  return '
    <div>
      Hello from Axel,<br /><br />
      You recently requested a password recovery for the 
      <strong>proVision Chrome Extension</strong> and <strong>proVision Rule Editor</strong>.<br />
      Your new password is:<br /><br />
      <strong>'.$pwd.'</strong><br /><br />
      You can log in to your <a href="https://www.axelproductions86.com/provision-rule-editor">profile</a> and change your password: 
      <a href="https://www.axelproductions86.com/provision-rule-editor">www.axelproductions86.com/provision-rule-editor</a>
    </div>
  ';
}