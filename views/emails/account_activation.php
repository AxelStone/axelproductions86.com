<?php 
function viewAccountActivationEmail($user) {

  return '
    <div>
      Hello <strong>'.$user['nickname'].'</strong> ;)<br /><br />
      Your account activation link is: <br />'.
      '<a href="https://www.axelproductions86.com/account-activation?code='.base64_encode($user['email']).'">
        www.axelproductions86.com/account-activation?code='.base64_encode($user['email']).
      '</a>'.
      '<br /><br />Welcome to the community and happy browsing!
    </div>
  ';
}