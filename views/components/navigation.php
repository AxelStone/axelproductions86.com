<?php

function viewNavigation() {

  $nav = '';
  foreach (NAVIGATION as $key => $value) {
    $nav .= '<div class="item"><a href="'.$value['url'].'">'.$value['title'].'</a></div>';
  }

  return $nav;

}