<?php

function viewFooter() {

  return '
    <footer>
      <div>
        <span class="copyright">&copy; 2021 All rights reserved.</span>
        <span class="creator">With&nbsp;&nbsp;<i class="fas fa-heart"></i>&nbsp;&nbsp;created by Team Axel</span>
      </div>
    </footer>
  ';
}