<?php

function viewRuleEditorHeader() {

  return '
  <header class="wrapper">
    <div>

      <a class="editorlogo" href="https://www.axelproductions86.com/provision-rule-editor" role="logo" title="proVision rule editor">
        <h1>
          <span class="icon" role="icon">{<span>..</span>}</span> 
          <div class="text">
            <div class="title"><strong>proVision</strong> <span>rule editor</span></div>
            <div class="descr">create, edit, suggest, vote..</div>
          </div>
        </h1>
      </a>
    
      <a href="https://www.axelproductions86.com" title="Axel Productions 86 - Homepage" role="logo">
        <h2 class="logo small">
          AXEL
        </h2>
      </a>

    </div>

  </header>';
}