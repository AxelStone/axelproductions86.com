<?php

function viewUpdates() {

  return '
    <div class="page sc">
      <h2>EXTENSION UPDATE: v.0.3.2</h2>
      <ul>
        <li>Ensuring that miniwidget stays always on top even if pop-up with maximal z-index is appended to the end of the &lt;body&gt; element.</li>
        <li>Improving set of allowed html elements to scan.</li>
        <li>Ensuring that found & hidden elements stay hidden even if their style attribute is additionaly overwritten by another website scripts.</li>
        <li>Fixing bugged indicator of cached page-specific rule count.</li>
        <li>Improving processing of &lt;body&gt; element in case of locked scrolling.</li>
        <li>Subtracting non-existent (additionaly removed/replaced) elements from the count of found elements.</li>
      </ul>
    </div>

    <div class="page sc">
      <h2>EXTENSION UPDATE: v.0.3.1</h2>
      <ul>
        <li>Fixing bugged common rules (for ".cc-*" classes) causing inputs for credit card info to disappear.</li>
        <li>Bulletproofing css for miniwidget that could be compromised by website\'s css.</li>
        <li>Changing default miniwidget settings to show miniwidget only when hidden elements are present.</li>
      </ul>
    </div>

    <div class="page sc">
      <h2>EXTENSION UPDATE: v.0.3.0</h2>
      <ul>
        <li>New feature! Adding "miniwidget" (small widget appearing at the bottom of browser window). 
          This allows easier access to extension\'s control functions especially for users that do not keep
          the extension\'s icon pinned in browser toolbar. 
        </li>
        <li>Adding settings for miniwidget (available in extension\'s window).</li>
        <li>Improving visual design in order to make the UI more smoother.</li>
        <li>Updating common rules / scanning algorithm.</li>
      </ul>
    </div>

    <div class="page sc">
      <h2>RULE EDITOR UPDATE: v.0.2.0</h2>
      <ul>
        <li>Rule editor has been approved by Google for the "Sign in by Google" (OAuth 2.0) feature and user accounts are fully working.</li> 
        <li>Basic CRUD operations over page-specific rules are available now and anybody can define rules for any website in order 
          to hide any popup or other annoying unwanted element not hidden by proVision Chrome Extension\'s automatic features.
        </li>
        <li>All the user-created rulesets will become valid after approval process. That means admins must check the user submission first. 
          The idea for the future is to shift this responsibility onto trusted community members.
        </li>
      </ul>
    </div>

    <div class="page sc">
      <h2>EXTENSION UPDATE: v.0.2.4</h2>
      <ul>
        <li>The new textual analysis feature from v.0.2.2 update does not scan the content inside an iframe element. An iframe is always a bad sign, 
          therefore if a suspicious element contains an iframe it will go hidden without further analysis.
        </li> 
        <li>The same applies for empty suspicious elements with position absolute or fixed (which is probably a semi-transparent page covering pop-up 
          overlay that does not contain an actual pop-up as a child element but stands besides the pop-up as a sibling).
        </li>
      </ul>
    </div>

    <div class="page sc">
      <h2>EXTENSION UPDATE: v.0.2.2</h2>
      <ul>
        <li>Implementing textual content analysis of "suspicious" HTML elements in order to better distinguish whether a given element 
          is just an innocent cookie recipe or an actual cookie consent bar/pop-up.
        </li> 
        <li>Improving common rule definitions for better results.
        </li>
        <li>Fixing minor UI translation bug.
        </li>
      </ul>
    </div>

    <div class="page sc">
      <h2>RULE EDITOR UPDATE: v.0.1.1</h2>
      <ul>
        <li>Implementing login/registration and user management system. It is possible to create an account locally or sign up 
          with Google via OAuth2.0 now, only for testing puproses (accounts will be probably deleted sooner or later). 
          The same accounts will be valid also for proVision Chrome Extension. Registration currently does not require email verification.
        </li> 
      </ul>
    </div>

    <div class="page sc">
      <h2>EXTENSION UPDATE: v.0.2</h2>
      <ul>
        <li>Page-specific rules are finally in our database and synced with client extensions using REST API.
        </li> 
        <li>Synced rules for visited websites are currently cached for 24h and up to 300 records (FIFO method). 
        </li>
        <li>Settings section is now available and contains a clear-cache button in order to delete all cached rules if needed for some reason.
        </li>
      </ul>
    </div>

    <div class="page sc">
      <h2>RULE EDITOR UPDATE: v.0.1.0</h2>
      <ul>
        <li>Page-specific rule editor GUI is opened on https://www.axelproductions86.com/provision-rule-editor in read-only mode. 
          This is where users will be able to manage and update the rules.
        </li> 
      </ul>
    </div>

    <div class="page sc">
      <h2>EXTENSION UPDATE: v.0.1.4</h2>
      <ul>
        <li>New feature! Adding 5 colorful indicators indicating the types of found/hidden elements (cookies, push, adblocker, subscribe, other).
        </li> 
        <li>Updating both common and page-specific rulesets.
        </li>
        <li>Occasionally the MutationObserver\'s callback seems like it doesn\'t fire on page load for some reasons, resulting in scanning routine 
          not being executed immediately. Fixing this by forced scan routine execution after page load if not already executed by MutationObserver. 
        </li>
        <li>Removing creepy-looking right eye blinking animation from the glasses icon in extension window.   
        </li>
      </ul>
    </div>

    <div class="page sc">
      <h2>EXTENSION UPDATE: v.0.1.3</h2>
      <ul>
        <li>Using MutationObserver to trigger the scan routine when something changes in HTML DOM instead of periodically performed scans, 
          which brings significant CPU usage drop (should use this from beginning).
        </li> 
        <li>Improving visibility check algorithm in order to exclude present, but invisible, hidden or zero height unwanted elements 
          from the "found elements count"
        </li>
        <li>Fixing grammary errors in Slovak and Czech language (hopefully :-)) 
        </li>
      </ul>
    </div>

    <div class="page sc">
      <h2>EXTENSION UPDATE: v.0.1.2</h2>
      <ul>
        <li>Adding support for more consent management platforms (common rules)
        </li> 
        <li>Changing UI font to Source Sans Pro
        </li>
        <li>Fixing minor UI bugs appearing in other languages
        </li>
        <li>Fixing wrong input placeholders in user feedback section 
        </li>
      </ul>
    </div>

    <div class="page sc">
      <h2>EXTENSION UPDATE: v.0.1.1</h2>
      <ul>
        <li>After excruciatingly long period of time the proVision Chrome extension has been finally approved by Google and a betaversion
          is available in Chrome Web Store now.
        </li> 
        <li>Looking forward to your user feedback so we can fine-tune our AI and start building a database for page-specific rules. 
          Cookie consents, push notifications and other popups will not bother us again.
        </li>
      </ul>
    </div>
  ';
}