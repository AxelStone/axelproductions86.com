<?php

function viewHeader() {

  $logInOut = Store::$user ?
    '<a class="logged-in" href="/log-out">Odhlášení</a>' : 
    '<a class="logged-out" href="/log-in">Přihlášení</a> | <a class="sign-in" href="/sign-up">Registrace</a>'; 

  return '
  <header>
    <div class="wrapper">
    
      <a href="https://www.axelproductions86.com" title="Axel Productions 86 - Homepage" role="logo">
        <h1 class="logo">
          AXEL<span class="full"> PRODUCTIONS 86</span>
        </h1>
        <span class="slogan">
          Transforming fun & useful ideas into live community driven projects
        </span>
      </a>

      <!-- nav class="panel">
        '.viewNavigation().'
        <div class="item">
          <div class="log">
            '.$logInOut.'
          </div>
        </div>
      </nav -->

    </div>

  </header>';
}