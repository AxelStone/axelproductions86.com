<?php

function viewPrivacyPolicy() {

  return viewAxelLayout('
      <article class="part privacy">
        <h2>
          <div class="imgwrap"><img alt="icon" src="assets/images/logo.svg" /></div>
          <div class="title">
            <div>Privacy policy</div>
            <div>For the <span>proVision Chrome Extension</span> and <span>proVision Rule Editor</span> website</div>
          </div>
        </h2>
        <div class="content">
          <p>
            This page informs you of our policies regarding the collection, use and disclosure of Personal Information we receive from users of:
          </p>
          <ul class="spaces">
            <li>the <strong>proVision Chrome Extension</strong> available on:<br> 
              <a href="https://chrome.google.com/webstore/detail/provision/napdgmfnfbebjgahggnalabkkfaajldf">
                chrome.google.com/webstore/detail/provision/napdgmfnfbebjgahggnalabkkfaajldf
              </a>
            </li>
            <li>and <strong>proVision Rule Editor</strong> website hosted on:<br>
              <a href="https://www.axelproductions86.com/provision-rule-editor">www.axelproductions86.com/provision-rule-editor</a>.
            </li>
          </ul> 
          <p>
            By using these services, you agree to the collection and use of information in accordance with this Privacy Policy.
          </p>

          <h3>Collected data</h3>
          <p>
            We do not share nor sell any collected personal user informations to third parties. All the informations we collect are willingly and knowingly 
            submitted by user\'s interaction, with addition of time of data submission. 
          </p>
          <p>
            We are providing the possiblity to create an user account locally (encrypted user password stored in our database) or by signing in with 
            Google account using OAuth 2.0 protocol (only Google account token is stored in our database and user\'s identity is verified by Google). 
            The same account is valid for both <strong>proVision Chrome Extension</strong> and <strong>proVision Rule Editor</strong> website.
          </p>
          <p>
            In case of user account registration (or login) we will collect this in our database:
            <ul>
              <li>e-mail (not visible to other users, used as username in login form)</li>
              <li>password (stored in encrypted form, original password is not known to us)</li>
              <li>nickname (in case of local account, may be visible to other users)</li>
              <li>first and last name from Google account (in case of signing in with Google, may be visible to other users)</li>
              <li>time of account registration (may be visible to other users)</li>
              <li>time of last login (not visible to other users)</li>
            </ul>
          </p>
          <p>
            We are also providing the possiblity to send user feedback from the <strong>proVision Chrome Extension</strong>. In this case we will collect:
            <ul>  
              <li>currently opened website URL (or user-defined URL, not visible to other users)</li>
              <li>optional e-mail address (for anonymous users, not visible to other users)</li>
              <li>feedback text (not visible to other users)</li>
              <li>time of submission (not visible to other users)</li>
            </ul>
          </p>
          <p>
            All the other data submitted by user interactions (posts, comments, etc.) is considered non-personal and in our ownership. 
            By submitting this data you agree that we are free to deal with the data in any way we wish.  
          </p>

          <h3>Cookies</h3>
          <p>
            We are using only necessary/essential cookies (in <strong>proVision Rule Editor</strong> website) containing user\'s access token 
            that allows us to authenticate the user. By using our services you agree to use of this cookie. We are not using any other cookies 
            (for example cookies containing user\'s preferences, activity or browsing history). In case of <strong>proVision Chrome Extension</strong>
            we are storing this token in Chrome\'s local storage. 
          </p>

          <h3>E-mails</h3>
          <p>
            We do not send any newsletters nor promotional material. All the e-mails users will recieve are necessary for user account management 
            purposes (account activation e-mail, password recovery e-mail or necessary notifications according to user actions and profile settings).
          </p>

          <h3>User tracking</h3>
          <p>        
            We do not use any tracking technologies that analyze user behaviour like Google Analytics and do not collect any informations, 
            that are not willingly and knowingly submitted by users (for example IP address, browser name, visited pages, user activity, 
            time and length of visit, etc.). 
          </p>

          <h3>Contact</h3>
          <p>
            If you want us to cancel your account and destroy all your personal informations stored in our database or have any questions about 
            this Privacy Policy, you can contact us by e-mail: <a href="mailto:axelproductions86@gmail.com">axelproductions86@gmail.com</a>
          </p>
        </div>
      </article>
  ');
}