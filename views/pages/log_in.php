<?php 

function viewLogIn() {

  $userDoesntExists = in_array(LogInErrs::UserDoesntExists, Store::$logInErrs) ?
    '<div class="error">Toto užívatelské jméno neexistuje!</div>' : ''; 

  $wrongPasword = in_array(LogInErrs::WrongPassword, Store::$logInErrs) ?
    '<div class="error">Nesprávne heslo!</div>' : ''; 

  return viewBasicLayout('

    <div class="page-title">
      <div class="box">
        <h1>Přihlašení</h1>
      </div>
    </div>
    
    <div class="form-block login">
      <form class="log-in" action="log-in" method="post">
        <div class="col">
          <div class="field username">
            <label for="username">Užívatelké jméno:</label>
            <input type="text" id="username" name="username" value="'.Helper::postVal('username').'">
            '.$userDoesntExists.'
          </div>
          <div class="field password">
            <label for="password">Heslo:</label>
            <input type="password" id="password" name="password" value="'.Helper::postVal('password').'">
            '.$wrongPasword.'
          </div>
          <div class="field actions">
            <a href="/sign-up">Založit nový účet</a>
            <input type="submit" value="Přihlasit">
          </div>
        </div>
      </form>
    </div>
  ');

}

