<?php 

function viewAccountActivation() {

  return viewAxelLayout('
    <div class="part activation">
      Welcome to the community, <strong>'.Store::$user['nickname'].'</strong>.<br/><br/>
      Your account has been activated!. <br>
      You may now log in to 
      <a href="https://www.axelproductions86.com/provision-rule-editor">proVision Rule Editor</a> and soon the same account<br />will be valid also for 
      <a href="https://chrome.google.com/webstore/detail/provision/napdgmfnfbebjgahggnalabkkfaajldf">proVision Chrome Extension</a>.
      <br /><br />
      <a href="https://www.axelproductions86.com">Back to homepage</a>
    </div>
  ');

}

