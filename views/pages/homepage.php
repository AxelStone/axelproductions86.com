<?php

function viewHomepage() {

  return viewAxelLayout('
    <div class="part">
      <div class="video">
        <video width="100%" height="auto" controls poster="assets/video/provision_videothumb.jpg">
          <source src="assets/video/provision.mov" type="video/mp4">
        Your browser does not support the video tag.
        </video>
      </div>

      <div class="badges row">
        <div class="extension">
          <a target="_blank" title="proVision Chrome Extension in Chrome Web Store" 
            href="https://chrome.google.com/webstore/detail/provision/napdgmfnfbebjgahggnalabkkfaajldf">
            <img src="assets/images/chrome_web_store_badge.png" alt="proVision Chrome Extension - available in Chrome Web Store" >
          </a>
          <div class="description">
            Get the <strong>proVision</strong> Chrome Extension<br>
            and experience popup-free undisturbed browsing
          </div>
        </div>
        <div class="editor">
          <a href="/provision-rule-editor" title="proVision Rule Editor - page-specific rule editor GUI">
            <span class="icon" role="icon">{<span>..</span>}</span> 
            <div class="text">
              <div class="descr">Create, edit, suggest, vote</div>
              <div class="title"><strong>proVision</strong> <span>Rule Editor</span></div>
            </div>
          </a>
          <div class="description">
            Visit <strong>proVision</strong> page-specific rule editor<br>
            <span class="light">and help us clear & maintain the world wide web</span>
          </div>
        </div>
      </div>

      <div class="art row">
        <div class="wrap">
          <div class="textual">
            <div class="label">CHROME EXTENSION</div>
            <div class="title">proVision</div>
            <div class="text">Community driven cookie consent<br>and other popups removal tool for Chrome.</div>
          </div>
          <div class="icon">
            <img src="assets/images/logo.svg" />
          </div>
          <div class="shadow"></div>
        </div>
      </div>

    </div>
    <div class="part tabs">

      <button id="button-about" class="tabbutton active">ABOUT</button>
      <button id="button-devlog" class="tabbutton">DEVLOG</button>

      <div id="tab-devlog" class="devlog slider hidden">
      '.viewUpdates().'
      </div>

      <div id="tab-about" class="about">

        <article class="content sc">
          <p>
            The General Data Protection Regulation (GDPR) is the toughest privacy and security law in the world. 
            Though it was drafted and passed by the European Union (EU), it imposes obligations onto organizations anywhere, 
            so long as they target or collect data related to people in the EU. The regulation was put into effect on May 25, 2018.
          </p>      
          <p>
            Since then, web is being flooded with annoying and obtrusive cookie consent bars and pop-ups requiring user interaction, 
            narrowing browser\'s viewport or covering page content. We have an idea to build a community and together deal with this 
            and other unnecessary stuff like push notifications, subscribe prompts or turn-off-your-adblocker messages once and for all. 
            There has not yet been a solution like this!
          </p>
          
          <h3>HOW DOES IT WORK?</h3>
          
          <ul>
            <li><p>
              We are testing and improving our algorithm (a set of common rules/checks performed above website HTML) 
              capable to recognize the unwanted content. That works for majority of websites, however in some cases 
              (especially giants like Facebook, Twitter, etc.) it is practically impossible to locate and hide the 
              right element automatically. That\'s why:
            </p></li>
            <li><p>
              We are building community responsible for provision and update of page-specific rules. 
              You can report any unwanted popups or elements that are not hidden by automatic features directly in extension\'s window 
              and developers will take care of your feedback. You can also visit our 
              <a href="https://www.axelproductions86.com/provision-rule-editor" title="proVision rule editor">page-specific rule editor</a>
              and define rules for any specific website by yourself. 
              After approval those rules will be synced with client extensions.
            </p></li>
          </ul>

          <h3>IS IT FREE?</h3>
          <p>
            Yes, core concept and basic functionality is and always will be free. However, there should be some reward system for active 
            contributing users. We are planning some additional unlockable features that could be alternatively unlocked with few bucks.
            Details are being discussed.
          </p>
          
          <h3>HELP US AND JOIN THE COMMUNITY!</h3>
          <p>
            Support the community and give us your feedback by clicking the yellow button with ! sign in extension window 
            and report websites where cookie conset bar or pop-up is not disappearing or any other issue you see.
          </p>
        </article>
      </div>

      <div class="art2">
        <img role="banner" alt="proVision - cookie consents, push notifications, subscribe prompts, 
          turn-off-your-adblocker popups and other annoying stuff removal tool. Driven by community." 
          src="assets/images/provision_800x320_banner.jpg" 
        />
      </div>
    </div>

    <div class="part last">

      <div class="privacy">
        Read our <a href="https://www.axelproductions86.com/privacy-policy"><strong>Privacy Policy</strong></a> 
        for both <strong>proVision Chrome Extension</strong> and <strong>proVision Rule Editor</strong> if you\'re interested
        in how we are dealing with user data. 
      </div>
    </div>
  ');

}