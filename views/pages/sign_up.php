<?php 

function viewSignUp() {

  $shortUsername = in_array(SignUpErrs::ShortUsername, Store::$signUpErrs) ?
    '<div class="error">Uživatelské jméno musí obsahovat aspoň '.USERNAME_MIN_LENGTH.' znaků!</div>' : ''; 

  $usernameAlreadyExists = in_array(SignUpErrs::UsernameAlreadyExists, Store::$signUpErrs) ?
    '<div class="error">Toto uživatelské jméno již existuje!</div>' : ''; 

  $shortPassword = in_array(SignUpErrs::ShortPassword, Store::$signUpErrs) ?
    '<div class="error">Heslo musí obsahovat aspoň '.PASSWORD_MIN_LENGTH.' znaků!</div>' : '';

  $passwordsDidntMatch = in_array(SignUpErrs::PasswordsDidntMatch, Store::$signUpErrs) ?
    '<div class="error">Hesla se neshodují!</div>' : ''; 

  $usertypeNotSelected = in_array(SignUpErrs::UserTypeNotSelected, Store::$signUpErrs) ?
    '<div class="error">Vyberte typ uživatele!</div>' : ''; 

  return viewBasicLayout('
    <div class="page-title">
      <div class="box">
        <h1>Vytvořit nový účet</h1>
      </div>
    </div>

    <div class="form-block signup">
      <form class="sign-up" action="sign-up" method="post">

        <div class="col">

          <div class="field username">
            <label for="username">Uživatelské jméno:</label>
            <input type="text" id="username" name="username" value="'.Helper::postVal('username').'">
            '.$shortUsername.$usernameAlreadyExists.'
          </div>

          <div class="field password">
            <label for="password">Heslo:</label>
            <input type="password" id="password" name="password" value="'.Helper::postVal('password').'">
            '.$shortPassword.'
          </div>

          <div class="field confirm-password">
            <label for="confirm_password">Potvrdit heslo:</label>
            <input type="password" id="confirm_password" name="confirm_password" value="'.Helper::postVal('confirm_password').'">
            '.$passwordsDidntMatch.'
          </div>

          <div class="field usertype">
            <div class="label">Typ užívatele:</div>
            <input type="radio" id="customer" name="usertype" value="1" '.(Helper::postVal('usertype') == "1" ? 'checked' : '').'>
            <label for="customer">Zákazník</label>
            <input type="radio" id="seller" name="usertype" value="2" '.(Helper::postVal('usertype') == "2" ? 'checked' : '').'>
            <label for="seller">Predajca</label>
            '.$usertypeNotSelected.'
          </div>

          <div class="field actions">
            <a href="/log-in">Přihlašení existujíciho uživatele</a>
            <input type="submit" value="Zaregistrovat">
          </div>
          
        </div>

      </form>
    </div>
  ');

}

