<?php 

function viewRuleEditorLayout($pageHtml) {

  return ' 
    <body class="rule-editor-layout">'.
      viewRuleEditorHeader().
      '<div class="page basic '.(Store::$user ? 'logged' : '').'">
        '.$pageHtml.'
      </div>'.
      viewFooter().'
    </body>';
}