<?php function viewAxelLayout($html) {
  return '
  
    <!--div id="stars2"></div>
    <div id="stars3"></div-->

    <div class="wrapper"> 
      <div class="page">

        <div class="side">

          <div class="logowrap">
            <a class="logo" href="https://www.axelproductions86.com" title="Axel Productions 86 - Homepage">
              <div class="axel">
                AXEL
              </div>
              <div class="productions">
                PRODUCTIONS <span>86</span>
              </div>
            </a>

            <div class="divider"></div><br>

            <div class="slogan">
              <span>Web development from the last millenium :)</span>
            </div>
          </div>

          <nav>
            <div class="header">SOCIAL</div>
            <div class="divider"></div>
            <div class="links social">
              <a class="item" href="https://www.facebook.com/axelproductions86" title="Facebook page">
                <i class="fab fa-facebook-f"></i>Facebook
              </a>
              <a class="item" href="https://www.linkedin.com/in/tomas-turan" title="LinkedIn profile">
                <i class="fab fa-linkedin-in"></i>LinkedIn
              </a>
              <a class="item" href="https://gitlab.com/AxelStone" title="GitLab account">
                <i class="fab fa-gitlab"></i>GitLab
              </a>
            </div>

            <div class="header">PROJECTS</div>
            <div class="divider"></div>
            <div class="links projects">
              <a class="item" href="https://www.axelproductions86.com" title="Cookie consent and other popups removal tool">
                <i class="fas fa-caret-right"></i>proVision: Chrome Extension
              </a>
              <a class="item" href="https://www.axelproductions86.com/provision-rule-editor" title="Rule editor for specific websites">
                <i class="fas fa-caret-right"></i>proVision: Rule Editor
              </a>
            </div>  
          </nav>
          
        </div>

        <main class="main">'.$html.'</main>

      </div>   
      
      '.viewFooter().'
    </div>
    
  ';
}