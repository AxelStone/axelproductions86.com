<?php 

function viewBasicLayout($pageHtml) {

  return ' 
    <body class="basic-layout">'.
      viewHeader().
      '<div class="page basic '.(Store::$user ? 'logged' : '').'">
        '.$pageHtml.'
      </div>'.
      viewFooter().'
    </body>';
}